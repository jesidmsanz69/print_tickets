const express = require("express");
const appExpress = express();
const { app, ipcMain, BrowserWindow } = require("electron");
const http = require("http");
const server = http.createServer(appExpress);
const socketio = require("socket.io");
const io = socketio(server);
const bodyParser = require("body-parser");
const getIpAddress = require("./utils/getIpLocalIPv4");
const PORT = 3050;
// const HOST = getIpAddress();
const HOST = "127.0.0.1";

// Analizar solicitudes JSON
appExpress.use(bodyParser.json());
appExpress.use(bodyParser.urlencoded({ extended: false }));

function createWindow() {
  const win = new BrowserWindow({
    width: 400,
    height: 450,
    resizable: false,
    maximizable: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
    autoHideMenuBar: true, //
  });
  // Carga la página index.html de la aplicación
  win.loadFile("index.html");

  // Abre las herramientas de desarrollo de Chrome
  win.webContents.openDevTools();
}

// Configuración del ticket_print
appExpress.post("/ticket_print", (req, res) => {
  const data = req.body;
  console.log("Recibido:", data);
  io.emit("data", { ...data, host: HOST });
  res.send({ message: "Solicitud recibida", data });
});

// Configuración del WebSocket
io.on("connection", (socket) => {
  console.log("Un cliente se ha conectado al servidor WebSocket");
});

// Inicia el servidor express y la aplicación de Electron
function start() {
  server.listen(PORT, HOST, () => {
    console.log(`Servidor corriendo en http://${HOST}:${PORT}`);
  });
  io.emit("data", { api: `http://${HOST}:${PORT}/ticket_print` });
  app.whenReady().then(createWindow).then(createBackgroundWindow);
}

start();
