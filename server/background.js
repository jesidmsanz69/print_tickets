const { BrowserWindow } = require("electron");

let backgroundWindow;

function createBackgroundWindow() {
  backgroundWindow = new BrowserWindow({
    show: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  backgroundWindow.loadFile("./utils/background.html");

  backgroundWindow.on("closed", () => {
    backgroundWindow = null;
  });
}

module.exports = { createBackgroundWindow };
