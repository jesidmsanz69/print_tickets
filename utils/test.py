import serial
import socket
import usb.core
import usb.util

from escpos.printer import Escpos


class Usb(Escpos):

    def __init__(
        self,
        idVendor,
        idProduct,
        usb_args=None,
        timeout=0,
        in_ep=0x82,
        out_ep=0x01,
        *args,
        **kwargs
    ):  # noqa: N803
        """
        :param idVendor: Vendor ID
        :param idProduct: Product ID
        :param usb_args: Optional USB arguments (e.g. custom_match)
        :param timeout: Is the time limit of the USB operation. Default without timeout.
        :param in_ep: Input end point
        :param out_ep: Output end point
        """
        Escpos.__init__(self, *args, **kwargs)
        self.timeout = timeout
        self.in_ep = in_ep
        self.out_ep = out_ep

        usb_args = usb_args or {}
        if idVendor:
            usb_args["idVendor"] = idVendor
        if idProduct:
            usb_args["idProduct"] = idProduct
        self.open(usb_args)


    def open(self, usb_args):
        self.device = usb.core.find(**usb_args)
        if self.device is None:
            raise print("Device not found or cable not plugged in.")

        self.idVendor = self.device.idVendor
        self.idProduct = self.device.idProduct

        if self.device.backend.__module__.endswith("libusb1"):
            check_driver = None

            try:
                check_driver = self.device.is_kernel_driver_active(0)
            except NotImplementedError:
                pass

            if check_driver is None or check_driver:
                try:
                    self.device.detach_kernel_driver(0)
                except NotImplementedError:
                    pass
                except usb.core.USBError as e:
                    if check_driver is not None:
                        print("Could not detatch kernel driver: {0}".format(str(e)))

        try:
            self.device.set_configuration()
            self.device.reset()
        except usb.core.USBError as e:
            print("Could not set configuration: {0}".format(str(e)))


    def _raw(self, msg):
        self.device.write(self.out_ep, msg, self.timeout)

    def _read(self):
        return self.device.read(self.in_ep, 16)

    def close(self):
        if self.device:
            usb.util.dispose_resources(self.device)
        self.device = None


