const { exec } = require("child_process");

function imprimirEnImpresoraPOS(obj) {
  const jsonData = JSON.stringify(obj).replace(/'/g, '"');
  exec(`python "${__dirname}/print.py" '${jsonData}'`, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error al imprimir en la impresora: ${error}`);
      return error;
    }
    console.log(`Texto impreso en la impresora: ${stdout}`);
  });
}

module.exports = imprimirEnImpresoraPOS;
