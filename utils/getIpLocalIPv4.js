const os = require("os");

function getIpAddress() {
  const ifaces = os.networkInterfaces();
  let ipAddress = null;

  Object.keys(ifaces).forEach((ifname) => {
    ifaces[ifname].forEach((iface) => {
      if (iface.family !== "IPv4" || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-IPv4 addresses
        return;
      }
      ipAddress = iface.address;
    });
  });

  return ipAddress;
}

module.exports = getIpAddress;
